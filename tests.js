test("BaseCases", function (assert) {
    assert.equal(isValidPlate("0000"+"BBB"), true, "Same leters");
    assert.equal(isValidPlate("1234"+"CDF"), true, "Different");
});

test("Not accepted letter", function (assert) {
    assert.equal(isValidPlate("0123"+"AAA"), false, "All vowels");
    assert.equal(isValidPlate("0123"+"BCE"), false, "One vowel");
    assert.equal(isValidPlate("0123"+"BCÑ"), false, "One ñ");
    assert.equal(isValidPlate("0123"+"BCQ"), false, "One q");
    assert.equal(isValidPlate("0123"+"qña"), false, "all not acepted letter");
});

test("Lower case", function (assert) {
    assert.equal(isValidPlate("0000"+"bbb"), true, "Same leters");
    assert.equal(isValidPlate("1234"+"cdf"), true, "Different");
    assert.equal(isValidPlate("0123"+"bce"), false, "One vowel");
});